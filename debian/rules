#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS := hardening=+all qa=+bug reproducible=+all

%:
	dh $@

# dh_autoreconf interferes with portability -- skip it
override_dh_autoreconf:
	echo "skip: dh_autoreconf autoreconf-dickey -- -f -i"

override_dh_auto_configure:
	dh_auto_configure -- \
		--enable-warnings \
		--verbose \
		--enable-stdnoreturn \
		--enable-btyacc \
		--program-transform-name='s,^yacc,byacc,'

	# workaround for whatis, consistent naming
	sed -i \
		-e '/^\.TH/s, YACC, BYACC,' \
		-e '/^\.ds/s, Yacc, Byacc,' \
		-e '/^\.ds/s, yacc, byacc,' \
		-e '/- an LALR/s,^..N,byacc,' \
		yacc.1

	# omit tests which rely upon getopt error-messages
	sed -i.bak '/MYFILE=nosuchfile/,/# Test special cases/d' test/run_test.sh
	diff -u test/run_test.sh.bak test/run_test.sh || echo OK
